require('dotenv').config();

// todo: switch your NODE_ENV to production when deploying to production environment
const CONFIG = {
    NODE_ENV: process.env.NODE_ENV || 'development',
    PORT: process.env.SERVICE_PORT || 3020,
    HOST: process.env.SERVICE_HOST || 'http://127.0.0.1',
    PATH: process.env.SERVICE_PATH || ':3020/stockopname',
    DB: {
        host: process.env.DB_HOST || '188.166.204.188',
        port: process.env.DB_PORT || 15445,
        user: process.env.DB_USER || 'postgres',
        pass: process.env.DB_PASS || 'h05p1TalP@ssw0rd!',
        dialect: process.env.DB_DIALECT || 'postgres',
        name: process.env.DB_NAME || 'postgres',
        schema: process.env.DB_SCHEMA || 'Hospital',
        pool: {
            max: process.env.DB_MAX_POOL || 5,
            min: process.env.DB_MIN_POOL || 0,
            acquire: process.env.DB_ACQUIRE || 30000,
            idle: process.env.DB_IDLE || 10000
        },
        define: {
            timestamps: false,
            freezeTableName: true
        }
    },
    REDIS: {
        PORT: process.env.REDIS_PORT || 6379,
        HOST: process.env.REDIS_HOST || '127.0.0.1',
        PASSWORD: process.env.REDIS_PASSWORD || ''
    },
    CERT: {
        PUBLIC_KEY: process.env.CERT_PUBLIC_KEY || './src/certs/public_key.pem',
        PRIVATE_KEY: process.env.CERT_PRIVATE_KEY || './src/certs/private_key.pem'
    }
};

module.exports = CONFIG;
