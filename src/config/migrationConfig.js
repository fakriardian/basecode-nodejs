const { NODE_ENV, DB } = require('.');

const cfg = {};
const conf = {};

conf.environment = NODE_ENV;
conf.sequelize = {};
conf.sequelize.username = DB.USER;
conf.sequelize.password = DB.PASSWORD;
conf.sequelize.database = DB.NAME;
conf.sequelize.host = DB.HOST.host;
conf.sequelize.dialect = DB.HOST.dialect;
conf.sequelize.port = DB.HOST.port;
conf.sequelize.define = {
    schema: 'Hospital',
    charset: 'utf8mb4',
    dialectOptions: {
        collate: 'utf8mb4_unicode_ci'
    }
};
conf.ROUND_SALT = 8;

cfg[conf.environment] = conf.sequelize;

module.exports = cfg;
