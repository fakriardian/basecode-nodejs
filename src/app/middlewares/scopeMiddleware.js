/* eslint-disable import/no-unresolved */

const { NODE_ENV } = require('src/config');
const logger = require('src/utils/logger')(__filename, NODE_ENV);

const { decodeToken } = require('src/utils/token/tokenOperations');

module.exports = (SCOPE_TYPE) => async (req, res, next) => {
    const { authorization } = req.headers;
    const accessToken = authorization.substring(7, authorization.length);
    const decodedToken = decodeToken(accessToken);
    const { scope: { scope } } = decodedToken;

    if (!scope || !scope.length > 0 || !Array.isArray(scope)) {
        logger.error('Forbidden Access 00x1 - Scope Undefined');
        return res.status(403).send('Forbidden Access!');
    }

    const upperCasedScopes = scope.map((el) => el.toUpperCase());
    if (upperCasedScopes.indexOf(SCOPE_TYPE) === -1) {
        logger.error('Forbidden Access 00x2 - Scope Unauthorized');
        return res.status(403).send('Forbidden Access!');
    }

    logger.info('Scope validated, passed to next handler');
    return next();
};
