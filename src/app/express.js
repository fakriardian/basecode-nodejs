const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const passport = require('passport');
const logger = require('../utils/logger');
const { middleware } = require('../utils/confLogger');

/** Import passport implemented strategies */
require('../utils/oauth/passport');

/** Import redis client */
require('../subcribers/redis/redis');

class App {
    /**
     * App Constructor
     * @param {Object} options - required parameters to run the app
     */
    constructor(options) {
        try {
            Object.assign(this, options);

            this.app = express();
            this.app.use(middleware());
            this.app.use('/static', express.static('static'));

            if (this.corsOptions) {
                this.app.use(cors());
                this.app.options('*', cors());
            }

            // todo: you can change /app with other name depends on the services functionality
            if (this.docs) {
                this.app.use('/app/swagger-ui', swaggerUi.serve, swaggerUi.setup(this.docs));
            }

            this.app.use(express.json());
            this.app.use(express.urlencoded({ extended: true }));
            this.app.use(helmet());

            const morganFormat = ':method :url :status :res[content-length] - :response-time ms';
            if (this.logger) {
                this.app.use(morgan(morganFormat, { stream: this.logger.stream }));
            }

            this.app.use(passport.authenticate('bearer', { session: false }));

            if (this.routes && this.routes.length !== 0) {
                // initInterceptor();
                this.app.use(this.routes);
            }

            // this.app.use(error);

            if (this.config.PORT) {
                const port = this.config.PORT;
                this.app.listen(port, () => logger.info(`Listening on port ${port}…`));
            }
            if (this.healthCheck) {
                this.healthCheck.checkDBService();
            }
        } catch (err) {
            logger.error(err.stack);
        }
    }
}

module.exports = App;
