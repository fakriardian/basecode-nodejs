const Sequelize = require('sequelize');
const { DB } = require('../../config');
const logger = require('../../utils/logger');

// todo: define your DB connection parameter here
const sequelize = new Sequelize(
    DB.name,
    DB.user,
    DB.pass,
    {
        host: DB.host,
        port: DB.port,
        dialect: DB.dialect,
        pool: DB.pool,
        define: DB.define,
        schema: DB.schema,
        logging: (msg) => logger.debug(msg)
    }
);

const database = {};
database.sequelize = sequelize;

module.exports = database;
