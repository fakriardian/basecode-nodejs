// eslint-disable-next-line import/no-unresolved
// eslint-disable-next-line import/no-unresolved
const logger = require('../../../utils/logger');
// eslint-disable-next-line import/no-unresolved
const models = require('../../models');

class HealthCheckService {
    // eslint-disable-next-line no-useless-constructor, no-empty-function
    // constructor() { }

    // eslint-disable-next-line class-methods-use-this
    async checkDBService() {
        let connection;
        try {
            connection = await models.sequelize.authenticate();
            logger.info('Database Connection has been established successfully.');
        } catch (error) {
            logger.error('Unable to connect to the database:', error);
        }
        return connection;
    }
}

module.exports = HealthCheckService;
